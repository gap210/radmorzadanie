//
// Created by grzegorz on 24.10.18.
//
#include <istream>
#include <fstream>
#include "include/gtest/gtest.h"
#include "include/gmock/gmock.h"
#include "Converter.h"

using testing::Eq;

namespace {
    class ConverterTest : public testing::Test {
    public:
        Converter converter;
        ConverterTest () {
            converter;
        }
    };

    /*
     * convertTxtToXml() tests
     */
    TEST_F(ConverterTest, convertTxtToXmlWithInvalidTxtPathShouldReturnFalse) {
        converter.setInputFilePath("");
        ASSERT_FALSE(converter.convertTxtToXml());
    }

    TEST_F(ConverterTest, convertTxtToXmlWithValidTxtAndXmlPathShouldReturnTrue) {
        converter.setInputFilePath("/home/grzegorz/CLionProjects/test.txt");
        converter.setOutputFilePath("/home/grzegorz/CLionProjects/test.xml");
        ASSERT_TRUE(converter.convertTxtToXml());
    }

    TEST_F(ConverterTest, txtToXmlConverterShouldFailWhenOutputFilePathIsEmpty) {
        converter.setInputFilePath("/home/grzegorz/CLionProjects/test.txt");
        converter.setOutputFilePath("");
        ASSERT_FALSE(converter.convertTxtToXml());
    }

    /*
     * convertTxtToXml() tests
     */

    TEST_F(ConverterTest, convertXmlToTxtWithInvalidTxtPathShouldReturnFalse) {
        converter.setInputFilePath("");
        ASSERT_FALSE(converter.convertXmlToTxt());
    }

    TEST_F(ConverterTest, convertXmlToTxtWithValidTxtAndXmlPathShouldReturnTrue) {
        converter.setOutputFilePath("/home/grzegorz/CLionProjects/test.txt");
        converter.setInputFilePath("/home/grzegorz/CLionProjects/test.xml");
        ASSERT_TRUE(converter.convertXmlToTxt());
    }

    TEST_F(ConverterTest, xmlToTxtConverterShouldFailWhenOutputFilePathIsEmpty) {
        converter.setInputFilePath("/home/grzegorz/CLionProjects/test.xml");
        converter.setOutputFilePath("");
        ASSERT_FALSE(converter.convertXmlToTxt());
    }

    /*
     * setOutputFilePathFromUI() tests
     */
    TEST_F(ConverterTest, setOutputFilePathFromUIShouldReturnFalseWhenInvalidFileTypeArgumentIsPassed) {
        std::istringstream iss("validPath");
        ASSERT_FALSE(converter.setOutputFilePathFromUI("invalidArgument", iss));
    }

    TEST_F(ConverterTest, setOutputFilePathShouldProperlySetConverterOutputPathAttribute) {
        std::istringstream iss("validPath");
        converter.setOutputFilePathFromUI("xml", iss);
        ASSERT_TRUE(converter.getOutputFilePath() == "validPath");
    }

    /*
     * setInputFilePathFromUI() tests
     */
    TEST_F(ConverterTest, setInputFilePathFromUIShouldReturnFalseWhenInvalidPathIsPassed) {
        std::istringstream iss("InvalidPath");
        ASSERT_FALSE(converter.setInputFilePathFromUI("xml", iss));
    }

    TEST_F(ConverterTest, setInputFilePathFromUIShouldReturnTrueWhenValidPathIsPassed) {
        std::istringstream iss("/home/grzegorz/CLionProjects/test.xml");
        ASSERT_TRUE(converter.setInputFilePathFromUI("xml", iss));
    }

    TEST_F(ConverterTest, setInputFilePathFromUIShouldReturnFalseWhenInvalidFileTypeArgumentIsPassed) {
        std::istringstream iss("/home/grzegorz/CLionProjects/test.xml");
        ASSERT_FALSE(converter.setInputFilePathFromUI("invalidArgument", iss));
    }
    TEST_F(ConverterTest, setInputFilePathShouldProperlySetConverterOutputPathAttribute) {
        std::istringstream iss("/home/grzegorz/CLionProjects/test.xml");
        converter.setInputFilePathFromUI("xml", iss);
        ASSERT_TRUE(converter.getInputFilePath() == "/home/grzegorz/CLionProjects/test.xml");
    }

    /*
     * File contents test txt to xml
     */
    TEST_F(ConverterTest, convertTxtToXmlShlouldCreateFileMatchingTheExpectedResult) {

        //when:
        converter.setOutputFilePath("/home/grzegorz/CLionProjects/test.txt");
        converter.setInputFilePath("/home/grzegorz/CLionProjects/test.xml");
        converter.convertTxtToXml();

        //turning template xml into string
        std::ifstream xmlTemplate;
        xmlTemplate.open("/home/grzegorz/CLionProjects/testTemplate.xml");//open the input file with correct expected contents
        std::stringstream xmlTemplateFileStringStream;
        xmlTemplateFileStringStream << xmlTemplate.rdbuf();//read the file
        std::string strTem = xmlTemplateFileStringStream.str();

        //turning actual output xml into string
        std::ifstream txtToXmlConversionOutput;
        txtToXmlConversionOutput.open("/home/grzegorz/CLionProjects/test.xml");//open the input file with actual conversion contents
        std::stringstream xmlConversionOutputStringStream;
        xmlConversionOutputStringStream << txtToXmlConversionOutput.rdbuf();//read the file
        std::string strOut = xmlTemplateFileStringStream.str();

        ASSERT_EQ(strTem, strOut);


    }
    /*
     * File contents test xml to txt
     */
    TEST_F(ConverterTest, convertXmlToTxtShlouldCreateFileMatchingTheExpectedResult) {

        //when:
        converter.setOutputFilePath("/home/grzegorz/CLionProjects/test.txt");
        converter.setInputFilePath("/home/grzegorz/CLionProjects/test.xml");
        converter.convertXmlToTxt();

        //turning template txt into string
        std::ifstream txtTemplate;
        txtTemplate.open("/home/grzegorz/CLionProjects/testTemplate.txt");//open the input file with correct expected contents
        std::stringstream txtTemplateFileStringStream;
        txtTemplateFileStringStream << txtTemplate.rdbuf();//read the file
        std::string strTem = txtTemplateFileStringStream.str();

        //turning actual output txt into string
        std::ifstream xmlToTxtConversionOutput;
        xmlToTxtConversionOutput.open("/home/grzegorz/CLionProjects/test.txt");//open the input file with actual conversion contents
        std::stringstream txtConversionOutputStringStream;
        txtConversionOutputStringStream << xmlToTxtConversionOutput.rdbuf();//read the file
        std::string strOut = txtConversionOutputStringStream.str();

        ASSERT_EQ(strTem, strOut);


    }






};
