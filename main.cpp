#include <iostream>
#include "lib/googletest-master/googletest/include/gtest/gtest.h"
#include "lib/googletest-master/googlemock/include/gmock/gmock.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unistd.h>
#include "/home/grzegorz/CLionProjects/pugixml-1.9/src/pugixml.hpp"
#include "Converter.h"

using namespace std;

//method returning user initial menu choice (int)
int userOptionChoice () {
    cout << "1 - zamien txt na xml" << endl << "2 - zamien xml na txt" << endl << "3 - uruchom testy i wyjdz" << endl;
    int choice;
    cin >> choice ;

    //check if user input is within desired values, invoke method again if not
    if (choice == 1 || choice == 2 || choice == 3) {
        return choice;
    } else if (std::cin.fail()){
        cout << "No i klops";
        exit(0);
    } else {
        userOptionChoice();
    }
    return choice;
}

int main(int argc, char* argv[])
{
    while (true) {

        //set user option choice from console input
        int choice = userOptionChoice();

        switch (choice) {
            case 1 : {
                //initialize a converter
                Converter toXmlConverter = Converter();

                //Take user input for the txt file to be read
                if(!toXmlConverter.setInputFilePathFromUI("txt", std::cin)){
                    cout << "Zla sciezka pliku txt" << endl;
                    break;
                };

                //set xml output path for converter using user input
                toXmlConverter.setOutputFilePathFromUI("xml", std::cin);

                //perform the conversion, return true if conversion successful, false if not
                if(!toXmlConverter.convertTxtToXml()) {
                    cout << "Cos sie nie powiodlo" << endl;
                } else {
                    cout << "Sukces" << endl;
                }
                break;
            }

            case 2 : {
                //initialize a converter
                Converter toTxtConverter = Converter();

                //Take user input for the xml file to be read
                if(!toTxtConverter.setInputFilePathFromUI("xml", std::cin)){
                    cout << "Zla sciezka pliku xml" << endl;
                    break;
                };
                //set txt output path for converter
                toTxtConverter.setOutputFilePathFromUI("txt", std::cin);

                //perform the conversion
                if (!toTxtConverter.convertXmlToTxt()) {
                    cout << "cos sie nie powiodlo" << endl;
                } else {
                    cout << "Sukces" << endl;
                }
                break;
            }

            case 3: {
                testing::InitGoogleTest(&argc, argv);
                return RUN_ALL_TESTS();
            }
            default:break;

        }
    }

}
