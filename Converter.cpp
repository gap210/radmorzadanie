#include <utility>

#include <utility>

//
// Created by grzegorz on 24.10.18.
//

#include <fcntl.h>
#include <fstream>
#include "Converter.h"
#include "pugixml.hpp"
#include "tests/Converter.h"

#include <vector>
#include <sstream>
#include <iostream>
#include <unistd.h>

using namespace std;

Converter::Converter(std::string newXmlFilePath, std::string newTxtFilePath) {
}
Converter::Converter() {
}
Converter::~Converter() {
}

const string &Converter::getInputFilePath() const {
    return inputFilePath;
}

void Converter::setInputFilePath(const string &inputFilePath) {
    Converter::inputFilePath = inputFilePath;
}

const string &Converter::getOutputFilePath() const {
    return outputFilePath;
}

void Converter::setOutputFilePath(const string &outputFilePath) {
    Converter::outputFilePath = outputFilePath;
}
/**
 * Method converting Xml to Txt by parsing xml file with pugixml external library methods.
 * Paths for both input and output files are determined by attributes of the converter
 * that the method is called on
 * @return true if conversion successfull, false otherwise
 */
bool Converter::convertXmlToTxt() {

    //loading xml file into memory
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(inputFilePath.c_str());

    //opening or creating a txt file to be written to
    fstream plik;
    plik.open(outputFilePath, ios::out);

    //checking if open/create of txt file was successful
    if (!plik.is_open()) {
        cout << "Cos jest nie tak z plikiem txt" << endl;
        return false;
    }

    //parsing xml <row> nodes
    for (pugi::xml_node nd:doc.first_child().children()) {

        //parsing <col> node children - text
        for (pugi::xml_node nd2:nd.children()) {

            //saving each text node to txt file followed by whitespace
            plik << nd2.first_child().value() << " ";
        }

        //new line after parsing through every <row>
        plik << endl;
    }

    plik.close();
    return true;
}

/**
 * Method converting a txt file to an xml file. Both files locations are determined by
 * attributes of the converter object that the method is called on.
 * @return true if conversion successfull, false otherwise
 */
bool Converter::convertTxtToXml() {

    // declaring input, output files, opening streams
    fstream plikTxt;
    fstream plikXml;
    plikTxt.open(inputFilePath, ios::in);
    plikXml.open(outputFilePath, ios::out);

    //checking if open/create was successful
    if (!plikXml.is_open()) {
        cout << "Cos jest nie tak z plikiem Xml" << endl;
        return false;
    } else if (!plikTxt.is_open()){
        cout << "Cos jest nie tak z plikiem Txt" << endl;
        return false;
    }

    //variable to hold currently read line
    string readLine;

    //adding xml header to plikXml
    plikXml << "<?xml version=\"1.0\"?>" << endl << "<root>" << endl;

    //looping while there are lines to read
    while (getline(plikTxt, readLine)) {

        //add <row> node for each line read
        plikXml << "<row>" << endl;

        //split read line into separate words with whitespace as a separator
        istringstream buf(readLine);
        istream_iterator<string> beg(buf), end;
        vector<string> tokens(beg, end);

        //add a column node for each of the read elements, after splitting line
        for (auto& s: tokens) {
            plikXml << "<col>" << s << "</col>" << endl;
        }

        //close the <row> node after processing line
        plikXml << "</row>" << endl;
    }
    //closing <root> node after looping through all lines
    plikXml << "</root>";

    plikTxt.close();
    plikXml.close();
    return true;
}

bool checkFileExists(const std::string &filePath) {
    int res = access(filePath.c_str(), R_OK);
    return res >= 0;
}
/**
 * method setting the output file path for the converter
 * @param fileType - arbitral string set to indicate if the user
 * gets asked for an xml or txt output file
 */
bool Converter::setOutputFilePathFromUI(std::string fileType, std::istream &input) {
    std::cout << "Podaj pelna sciezke pliku " + fileType + " do zapisu: " ;
    std::string pathStr;
    input >> pathStr;

    //checking if data provided as as filetype attr is not misleading
    if (fileType == "xml"){
        Converter::setOutputFilePath(pathStr);
    } else if (fileType == "txt") {
        Converter::setOutputFilePath(pathStr);
    } else {
        return false;
    }
    return true;
}
/**
 * method setting the output file path for the converter
 * @param fileType - arbitral string set to indicate if the user
 * gets asked for an xml or txt input file
 */
bool Converter::setInputFilePathFromUI(std::string fileType, std::istream &input) {
    cout << "Podaj pelna sciezke pliku " + fileType + " do odczytu: " ;
    string pathStr;
    input >> pathStr;

    //checking if data provided as as filetype attr is not misleading
    if (fileType == "xml"){
        Converter::setInputFilePath(pathStr);
    } else if (fileType == "txt") {
        Converter::setInputFilePath(pathStr);
    } else {
        return false;
    }
    //check if input file exists, no point in proceeding if it doesnt
    return checkFileExists(inputFilePath);
}

























