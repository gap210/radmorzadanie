//
// Created by grzegorz on 24.10.18.
//

#ifndef UNTITLED_TXTCONVERTER_H
#define UNTITLED_TXTCONVERTER_H


#include "pugixml.hpp"

class Converter {

private:

    std::string inputFilePath ;
    std::string outputFilePath ;

public:

    Converter();
    Converter(std::string inputFilePath, std::string outputFilePath);
    ~Converter();

    const std::string &getInputFilePath() const;
    void setInputFilePath(const std::string &inputFilePath);
    const std::string &getOutputFilePath() const;
    void setOutputFilePath(const std::string &outputFilePath);


    bool convertXmlToTxt ();
    bool convertTxtToXml ();
    bool setOutputFilePathFromUI(std::string fileType, std::istream &input);
    bool setInputFilePathFromUI(std::string fileType, std::istream &input);
};


#endif //UNTITLED_TXTCONVERTER_H
